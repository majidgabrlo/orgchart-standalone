import React from 'react'
import CustomNodeChart from './Custom-node-chart'
import { TransformWrapper, TransformComponent } from 'react-zoom-pan-pinch';


function App() {
  return (

    <TransformWrapper
      initialScale={1}
      initialPositionX={200}
      initialPositionY={100}
    >
      {({ zoomIn, zoomOut, resetTransform, ...rest }) => (
        <React.Fragment>
          <div className="tools">
            <button onClick={() => zoomIn()}>+</button>
            <button onClick={() => zoomOut()}>-</button>
            <button onClick={() => resetTransform()}>x</button>
          </div>
          <TransformComponent>
            <CustomNodeChart />
          </TransformComponent>
        </React.Fragment>
      )}
    </TransformWrapper>

  );
}

export default App;
