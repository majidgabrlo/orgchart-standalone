import React from "react";
import PropTypes from "prop-types";
import "./my-node.css";

const propTypes = {
  nodeData: PropTypes.object.isRequired
};

const MyNode = ({ nodeData }) => {
  const selectNode = () => {
    alert("Hi All. I'm " + nodeData.name + ". I'm a " + nodeData.title + ".");
  };

  return (
    <div onClick={selectNode}>
      <div className="position">{nodeData.name}</div>
      <div className="fullname">{nodeData.key}</div>
      
    </div>
  );
};

MyNode.propTypes = propTypes;

export default MyNode;
