import React from "react";
import OrganizationChart from "./components/ChartContainer";
import MyNode from "./my-node";

const CustomNodeChart = () => {
  const ds = {
    "id": 1,
    "name": "CEO",
    "key": "CEO",
    "description": "responsible for managing a company's overall operations",
    "parent_id": null,
    "tenant_id": null,
    "created_at": "2021-12-06T16:20:03.000000Z",
    "updated_at": "2021-12-06T16:20:03.000000Z",
    "children": [
        {
            "id": 2,
            "name": "Tech",
            "key": "Tech",
            "description": null,
            "parent_id": 1,
            "tenant_id": null,
            "created_at": "2021-12-06T16:20:03.000000Z",
            "updated_at": "2021-12-06T16:20:03.000000Z",
            "children": [
                {
                    "id": 3,
                    "name": "Dev Team manager",
                    "key": "DEV_TEAM_MANAGER",
                    "description": null,
                    "parent_id": 2,
                    "tenant_id": null,
                    "created_at": "2021-12-06T16:20:04.000000Z",
                    "updated_at": "2021-12-06T16:20:04.000000Z",
                    "children": [
                        {
                            "id": 4,
                            "name": "Dev Team",
                            "key": "DEV_TEAM",
                            "description": null,
                            "parent_id": 3,
                            "tenant_id": null,
                            "created_at": "2021-12-06T16:20:04.000000Z",
                            "updated_at": "2021-12-06T16:20:04.000000Z",
                            "children": []
                        }
                    ]
                }
            ]
        },
        {
            "id": 5,
            "name": "Product",
            "key": "PRODUCT",
            "description": null,
            "parent_id": 1,
            "tenant_id": null,
            "created_at": "2021-12-06T16:20:04.000000Z",
            "updated_at": "2021-12-06T16:20:04.000000Z",
            "children": [
                {
                    "id": 6,
                    "name": "Opr Manager",
                    "key": "OPR_MANAGER",
                    "description": null,
                    "parent_id": 5,
                    "tenant_id": null,
                    "created_at": "2021-12-06T16:20:05.000000Z",
                    "updated_at": "2021-12-06T16:20:05.000000Z",
                    "children": []
                },
                {
                    "id": 7,
                    "name": "Srv Manager",
                    "key": "SRV_MANAGER",
                    "description": null,
                    "parent_id": 5,
                    "tenant_id": null,
                    "created_at": "2021-12-06T16:20:05.000000Z",
                    "updated_at": "2021-12-06T16:20:05.000000Z",
                    "children": [
                        {
                            "id": 8,
                            "name": "Srv team",
                            "key": "SRV_TEAM",
                            "description": null,
                            "parent_id": 7,
                            "tenant_id": null,
                            "created_at": "2021-12-06T16:20:05.000000Z",
                            "updated_at": "2021-12-06T16:20:05.000000Z",
                            "children": []
                        }
                    ]
                },
                {
                    "id": 9,
                    "name": "Product Manager",
                    "key": "PRODUCT_MANAGER",
                    "description": null,
                    "parent_id": 5,
                    "tenant_id": null,
                    "created_at": "2021-12-06T16:20:05.000000Z",
                    "updated_at": "2021-12-06T16:20:05.000000Z",
                    "children": []
                }
            ]
        },
        {
            "id": 10,
            "name": "Administrative",
            "key": "ADMINISTRATIVE",
            "description": null,
            "parent_id": 1,
            "tenant_id": null,
            "created_at": "2021-12-06T16:20:05.000000Z",
            "updated_at": "2021-12-06T16:20:05.000000Z",
            "children": [
                {
                    "id": 11,
                    "name": "CEO Assistant",
                    "key": "CEO_ASSISTANT",
                    "description": null,
                    "parent_id": 10,
                    "tenant_id": null,
                    "created_at": "2021-12-06T16:20:06.000000Z",
                    "updated_at": "2021-12-06T16:20:06.000000Z",
                    "children": []
                },
                {
                    "id": 12,
                    "name": "Secretary",
                    "key": "SECRETARY",
                    "description": null,
                    "parent_id": 10,
                    "tenant_id": null,
                    "created_at": "2021-12-06T16:20:06.000000Z",
                    "updated_at": "2021-12-06T16:20:06.000000Z",
                    "children": []
                },
                {
                    "id": 13,
                    "name": "HR manager",
                    "key": "HR_MANAGER",
                    "description": null,
                    "parent_id": 10,
                    "tenant_id": null,
                    "created_at": "2021-12-06T16:20:06.000000Z",
                    "updated_at": "2021-12-06T16:20:06.000000Z",
                    "children": []
                },
                {
                    "id": 14,
                    "name": "IT manager",
                    "key": "IT_MANAGER",
                    "description": null,
                    "parent_id": 10,
                    "tenant_id": null,
                    "created_at": "2021-12-06T16:20:06.000000Z",
                    "updated_at": "2021-12-06T16:20:06.000000Z",
                    "children": []
                },
                {
                    "id": 15,
                    "name": "Support Manager",
                    "key": "SUPPORT_MANAGER",
                    "description": null,
                    "parent_id": 10,
                    "tenant_id": null,
                    "created_at": "2021-12-06T16:20:06.000000Z",
                    "updated_at": "2021-12-06T16:20:06.000000Z",
                    "children": []
                },
                {
                    "id": 16,
                    "name": "Finance Manager",
                    "key": "FINANCE_MANAGER",
                    "description": null,
                    "parent_id": 10,
                    "tenant_id": null,
                    "created_at": "2021-12-06T16:20:06.000000Z",
                    "updated_at": "2021-12-06T16:20:06.000000Z",
                    "children": []
                },
                {
                    "id": 17,
                    "name": "PMO Manager",
                    "key": "PMO_MANAGER",
                    "description": null,
                    "parent_id": 10,
                    "tenant_id": null,
                    "created_at": "2021-12-06T16:20:07.000000Z",
                    "updated_at": "2021-12-06T16:20:07.000000Z",
                    "children": []
                }
            ]
        },
        {
            "id": 18,
            "name": "Business",
            "key": "BUSINESS",
            "description": null,
            "parent_id": 1,
            "tenant_id": null,
            "created_at": "2021-12-06T16:20:07.000000Z",
            "updated_at": "2021-12-06T16:20:07.000000Z",
            "children": [
                {
                    "id": 19,
                    "name": "Sell",
                    "key": "SELL",
                    "description": null,
                    "parent_id": 18,
                    "tenant_id": null,
                    "created_at": "2021-12-06T16:20:07.000000Z",
                    "updated_at": "2021-12-06T16:20:07.000000Z",
                    "children": []
                },
                {
                    "id": 20,
                    "name": "Marketing",
                    "key": "MARKETING",
                    "description": null,
                    "parent_id": 18,
                    "tenant_id": null,
                    "created_at": "2021-12-06T16:20:08.000000Z",
                    "updated_at": "2021-12-06T16:20:08.000000Z",
                    "children": []
                },
                {
                    "id": 21,
                    "name": "R&I",
                    "key": "R&I",
                    "description": null,
                    "parent_id": 18,
                    "tenant_id": null,
                    "created_at": "2021-12-06T16:20:08.000000Z",
                    "updated_at": "2021-12-06T16:20:08.000000Z",
                    "children": []
                }
            ]
        }
    ]
}

  return <OrganizationChart datasource={ds} chartClass="myChart" NodeTemplate={MyNode} />;
};

export default CustomNodeChart;
